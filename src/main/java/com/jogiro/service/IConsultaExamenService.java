package com.jogiro.service;

import java.util.List;

import com.jogiro.model.ConsultaExamen;

public interface IConsultaExamenService {

	List<ConsultaExamen> listarExamenesPorConsulta(Integer idConsulta);
	
}
