package com.jogiro.service;

import com.jogiro.model.Archivo;

public interface IArchvoService {

	int guardar(Archivo archivo);
	Archivo leerArchivo(Integer idArchivo);
	
}
