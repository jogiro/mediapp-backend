package com.jogiro.service;

import java.util.List;

import com.jogiro.dto.ConsultaListaExamenDTO;
import com.jogiro.dto.ConsultaResumenDTO;
import com.jogiro.dto.FiltroConsultaDTO;
import com.jogiro.model.Consulta;

public interface IConsultaService extends ICRUD<Consulta>{

	Consulta registrarTransaccional(ConsultaListaExamenDTO consultaListaExamenDTO);
	
	List<Consulta> buscar(FiltroConsultaDTO filtro);
	
	List<Consulta> buscarFecha(FiltroConsultaDTO filtro);
	
	List<ConsultaResumenDTO> listarResumen();
	
	byte[] generarReporte();
	
	
}
