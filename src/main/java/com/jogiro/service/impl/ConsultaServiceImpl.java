package com.jogiro.service.impl;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jogiro.dto.ConsultaListaExamenDTO;
import com.jogiro.dto.ConsultaResumenDTO;
import com.jogiro.dto.FiltroConsultaDTO;
import com.jogiro.model.Consulta;
import com.jogiro.repo.IConsultaExamenRepo;
import com.jogiro.repo.IConsultaRepo;
import com.jogiro.service.IConsultaService;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class ConsultaServiceImpl implements IConsultaService{

	@Autowired
	private IConsultaRepo repo;
	
	@Autowired
	private IConsultaExamenRepo repoCE;
	
	@Transactional
	@Override
	public Consulta registrarTransaccional(ConsultaListaExamenDTO consultaListaExamenDTO) {
		
		consultaListaExamenDTO.getConsulta().getDetalleConsulta().forEach(det -> det.setConsulta(consultaListaExamenDTO.getConsulta()));
		
		repo.save(consultaListaExamenDTO.getConsulta());
		
		consultaListaExamenDTO.getListaExamen().forEach(e -> repoCE.registrar(consultaListaExamenDTO.getConsulta().getIdConsulta(), e.getIdExamen()));
		
		return consultaListaExamenDTO.getConsulta();
	}

	@Override
	public Consulta registrar(Consulta t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Consulta modificar(Consulta t) {
		return repo.save(t);
	}

	@Override
	public void eliminar(int id) {
		repo.deleteById(id);
		
	}

	@Override
	public Consulta listarId(int id) {
		Optional<Consulta> oc = repo.findById(id);
		return oc.isPresent() ? oc.get() : null;
	}

	@Override
	public List<Consulta> listar() {
		return repo.findAll();
	}

	@Override
	public List<Consulta> buscar(FiltroConsultaDTO filtro) {
		return repo.buscar(filtro.getDni(), filtro.getNombreCompleto());
	}

	@Override
	public List<Consulta> buscarFecha(FiltroConsultaDTO filtro) {
		LocalDateTime fechaSgte = filtro.getFechaConsulta().plusDays(1);
		return repo.buscarFecha(filtro.getFechaConsulta(), fechaSgte);
	}

	@Override
	public List<ConsultaResumenDTO> listarResumen() {
		List<ConsultaResumenDTO> consultasResumen = new ArrayList<>();
		repo.listarResumen().forEach(x -> {
			ConsultaResumenDTO cr = new ConsultaResumenDTO();
			cr.setCantidad(Integer.parseInt(String.valueOf(x[0])));
			cr.setFecha(String.valueOf(x[1]));
			consultasResumen.add(cr);
		});
		return consultasResumen;
	}

	@Override
	public byte[] generarReporte() {
		byte[] data = null;
		//HashMap<String, String> params = new HashMap<String, String>
		//params.put("txt.jasper", "jogiro");
		//Este HashMap se reemplaza este en el null del JasperPrint y de forma 
		//dinámica se asigna un valor en el Jasper generado
		
		try {
			File file = new ClassPathResource("/reports/consultas.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(this.listarResumen()));
			data = JasperExportManager.exportReportToPdf(print);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}
	
}
