package com.jogiro.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jogiro.model.Archivo;
import com.jogiro.repo.IArchivoRepo;
import com.jogiro.service.IArchvoService;

@Service
public class ArchivoServiceImpl implements IArchvoService{

	@Autowired
	private IArchivoRepo repo;

	@Override
	public int guardar(Archivo archivo) {
		Archivo ar = repo.save(archivo);
		return ar.getIdArchivo() > 0 ? 1: 0;
	}

	@Override
	public Archivo leerArchivo(Integer idArchivo) {
		Optional<Archivo> oa = repo.findById(idArchivo);
		return oa.isPresent() ? oa.get() : null;
	}
	
}
