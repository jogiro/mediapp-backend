package com.jogiro.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jogiro.model.Medico;
import com.jogiro.repo.IMedicoRepo;
import com.jogiro.service.IMedicoService;

@Service
public class MedicoServiceImpl implements IMedicoService{
	
	@Autowired
	private IMedicoRepo repo;

	@Override
	public Medico registrar(Medico t) {
		return repo.save(t);
	}

	@Override
	public Medico modificar(Medico t) {
		return repo.save(t);
	}

	@Override
	public void eliminar(int id) {
		repo.deleteById(id);		
	}

	@Override
	public Medico listarId(int id) {
		Optional<Medico> om = repo.findById(id);
		return om.isPresent() ? om.get() : null;
	}

	@Override
	public List<Medico> listar() {
		return repo.findAll();
	}
	
	
}
