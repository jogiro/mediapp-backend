package com.jogiro.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.jogiro.model.Paciente;
import com.jogiro.repo.IPacienteRepo;
import com.jogiro.service.IPacienteService;

@Service
public class PacienteServiceImpl implements IPacienteService{

	@Autowired
	private IPacienteRepo repo;

	@Override
	public Paciente registrar(Paciente t) {
		return repo.save(t);
	}

	@Override
	public Paciente modificar(Paciente t) {
		return repo.save(t);
	}

	@Override
	public void eliminar(int id) {
		repo.deleteById(id);
	}

	@Override
	public Paciente listarId(int id) {
		Optional<Paciente> op = repo.findById(id);
		return op.isPresent() ? op.get() : null;
	}

	@Override
	public List<Paciente> listar() {
		return repo.findAll();
	}

	@Override
	public Page<Paciente> listarPageable(Pageable pageable) {
		return repo.findAll(pageable);
	}
	
}
