package com.jogiro.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jogiro.model.Examen;
import com.jogiro.repo.IExamenRepo;
import com.jogiro.service.IExamenService;

@Service
public class IExamenServiceImpl implements IExamenService {

	@Autowired
	private IExamenRepo repo;

	@Override
	public Examen registrar(Examen t) {
		return repo.save(t);
	}

	@Override
	public Examen modificar(Examen t) {
		return repo.save(t);
	}

	@Override
	public void eliminar(int id) {
		repo.deleteById(id);
	}

	@Override
	public Examen listarId(int id) {
		Optional<Examen> oe = repo.findById(id);
		return oe.isPresent() ? oe.get() : null;
	}

	@Override
	public List<Examen> listar() {
		return repo.findAll();
	}

}
