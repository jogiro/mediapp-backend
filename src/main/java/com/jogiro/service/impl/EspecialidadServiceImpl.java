package com.jogiro.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jogiro.model.Especialidad;
import com.jogiro.repo.IEspecialidadRepo;
import com.jogiro.service.IEspecialidadService;

@Service
public class EspecialidadServiceImpl implements IEspecialidadService{

	@Autowired
	private IEspecialidadRepo repo;

	@Override
	public Especialidad registrar(Especialidad t) {
		return repo.save(t);
	}

	@Override
	public Especialidad modificar(Especialidad t) {
		return repo.save(t);
	}

	@Override
	public void eliminar(int id) {
		repo.deleteById(id);
	}

	@Override
	public Especialidad listarId(int id) {
		Optional<Especialidad> oe = repo.findById(id);
		return oe.isPresent() ? oe.get() : null;
	}

	@Override
	public List<Especialidad> listar() {
		return repo.findAll();
	}

	
	
}
