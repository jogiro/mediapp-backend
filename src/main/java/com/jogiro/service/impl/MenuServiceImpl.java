package com.jogiro.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jogiro.model.Menu;
import com.jogiro.repo.IMenuRepo;
import com.jogiro.service.IMenuService;

@Service
public class MenuServiceImpl implements IMenuService{

	@Autowired
	private IMenuRepo repo;

	@Override
	public Menu registrar(Menu t) {
		return repo.save(t);
	}

	@Override
	public Menu modificar(Menu t) {
		return repo.save(t);
	}

	@Override
	public void eliminar(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Menu listarId(int id) {
		Optional<Menu> om = repo.findById(id);
		return om.isPresent() ? om.get() : null;
	}

	@Override
	public List<Menu> listar() {
		return repo.findAll();
	}

	@Override
	public List<Menu> listarMenuPorUsuario(String nombre) {
		List<Menu> menus = new ArrayList<>();
		repo.listarMenuPorUsuario(nombre).forEach( x -> {
			Menu m = new Menu();
			m.setIdMenu(Integer.parseInt(String.valueOf(x[0])));
			m.setIcono(String.valueOf(x[1]));
			m.setNombre(String.valueOf(x[2]));
			m.setUrl(String.valueOf(x[3]));
			
			menus.add(m);
		});
		return menus;
	}
	
	
}
