package com.jogiro.service;

import java.util.List;

import com.jogiro.model.Menu;

public interface IMenuService extends ICRUD<Menu> {

	List<Menu> listarMenuPorUsuario(String nombre);
	
}
