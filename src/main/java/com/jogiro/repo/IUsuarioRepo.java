package com.jogiro.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jogiro.model.Usuario;

public interface IUsuarioRepo extends JpaRepository<Usuario, Long>{

	Usuario findOneByUsername(String username);
	
}
