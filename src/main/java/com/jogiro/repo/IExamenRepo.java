package com.jogiro.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jogiro.model.Examen;

public interface IExamenRepo extends JpaRepository<Examen, Integer>{

}
