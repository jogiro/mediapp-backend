package com.jogiro.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jogiro.model.Paciente;

public interface IPacienteRepo extends JpaRepository<Paciente, Integer>{

}
