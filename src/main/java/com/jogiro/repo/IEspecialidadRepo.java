package com.jogiro.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jogiro.model.Especialidad;

public interface IEspecialidadRepo extends JpaRepository<Especialidad, Integer>{

}
