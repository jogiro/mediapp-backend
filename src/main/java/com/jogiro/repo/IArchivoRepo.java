package com.jogiro.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jogiro.model.Archivo;

public interface IArchivoRepo extends JpaRepository<Archivo, Integer>{

}
