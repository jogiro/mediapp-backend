package com.jogiro.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jogiro.model.Medico;

public interface IMedicoRepo extends JpaRepository<Medico, Integer>{

}
