package com.jogiro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Información de examen médico")
@Entity
@Table(name = "examen")
public class Examen {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idExamen;
	
	@ApiModelProperty(notes = "El nombre del examen debe tener mínimo 3 caracteres")
	@Size(min = 3, message = "El nombre del examen debe tener mínimo 3 caracteres")
	@Column(name = "nombre", nullable = false, length = 50 )
	private String nombre;
	
	@ApiModelProperty(notes = "La descripción debe tener mínimo 3 caracteres")
	@Size(min = 3, message = "La descripción debe tener mínimo 3 caracteres")
	@Column(name = "descripcion", nullable = false, length = 250)
	private String descripcion;

	public Integer getIdExamen() {
		return idExamen;
	}

	public void setIdExamen(Integer idExamen) {
		this.idExamen = idExamen;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idExamen == null) ? 0 : idExamen.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Examen other = (Examen) obj;
		if (idExamen == null) {
			if (other.idExamen != null)
				return false;
		} else if (!idExamen.equals(other.idExamen))
			return false;
		return true;
	}
	
}
