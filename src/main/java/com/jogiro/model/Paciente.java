package com.jogiro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Información del paciente")
@Entity
@Table(name = "paciente")
public class Paciente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idPaciente;

	@ApiModelProperty(notes = "Los nombres deben tener mínimo 3 caracteres")
	@Size(min = 3, message = "Los nombres deben tener mínimo 3 caracteres")
	@Column(name = "nombres", nullable = false, length = 70)
	private String nombres;

	@ApiModelProperty(notes = "Los apellidos deben tener mínimo 3 caracteres")
	@Size(min = 3, message = "Los apellidos deben tener mínimo 3 caracteres")
	@Column(name = "apellidos", nullable = false, length = 70)
	private String apellidos;

	@ApiModelProperty(notes = "El dni debe tener entre 3 y 12 caracteres")
	@Size(min = 3, max = 12, message = "El dni debe tener entre 3 y 12 caracteres")
	@Column(name = "dni", nullable = false, length = 12)
	private String dni;

	@ApiModelProperty(notes = "La dirección debe tener máximo 150 caracteres")
	@Size(max = 150, message = "La dirección debe tener máximo 150 caracteres")
	@Column(name = "direccion", nullable = true, length = 150)
	private String direccion;

	@ApiModelProperty(notes = "El teléfono debe tener máximo 10 digitos")
	@Size(max = 10, message = "El teléfono debe tener entre 7 y 10 digitos")
	@Column(name = "telefono", nullable = true, length = 10)
	private String telefono;

	@ApiModelProperty(notes = "Email valido")
	@Email
	private String email;

	public Integer getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(Integer idPaciente) {
		this.idPaciente = idPaciente;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
