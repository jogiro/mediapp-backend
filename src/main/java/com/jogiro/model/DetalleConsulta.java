package com.jogiro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Información del detalle de cada consulta")
@Entity
@Table(name = "detalle_consulta")
public class DetalleConsulta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idDetalle;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "id_consulta", nullable = false, foreignKey = @ForeignKey(name = "fk_detalle_consulta_consulta"))
	private Consulta consulta;

	@ApiModelProperty(notes = "EL diagnóstico debe tener entre 3 y 70 caracteres")
	@Size(min = 3, max = 70, message = "EL diagnóstico debe tener entre 3 y 70 caracteres")
	@Column(name = "diagnostico", nullable = false, length = 70)
	private String diagnostico;

	@ApiModelProperty(notes = "El tratamiento debe tener entre 3 y 300 caracteres")
	@Size(min = 3, max = 300, message = "El tratamiento debe tener entre 3 y 300 caracteres")
	@Column(name = "tratamiento", nullable = false, length = 70)
	private String tratamiento;

	public Integer getIdDetalle() {
		return idDetalle;
	}

	public void setIdDetalle(Integer idDetalle) {
		this.idDetalle = idDetalle;
	}

	public Consulta getConsulta() {
		return consulta;
	}

	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public String getTratamiento() {
		return tratamiento;
	}

	public void setTratamiento(String tratamiento) {
		this.tratamiento = tratamiento;
	}

}
