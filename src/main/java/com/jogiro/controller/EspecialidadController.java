package com.jogiro.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.jogiro.exception.ModeloNotFoundException;
import com.jogiro.model.Especialidad;
import com.jogiro.service.IEspecialidadService;

@RestController
@RequestMapping("/especialidades")
public class EspecialidadController {

	@Autowired
	private IEspecialidadService service;
	
	@GetMapping
	public ResponseEntity<List<Especialidad>> listar(){
		
		List<Especialidad> especialidades = new ArrayList<>();
		especialidades = service.listar();
		return new ResponseEntity<List<Especialidad>>(especialidades, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Especialidad> listarId(@PathVariable("id") Integer id){
		
		Especialidad esp = new Especialidad();
		esp = service.listarId(id);
		if(esp == null) {
			throw new ModeloNotFoundException("Id no encontrado: " + id);
		}
		
		return new ResponseEntity<Especialidad>(esp, HttpStatus.OK);
	}	
	
	@PostMapping
	public ResponseEntity<Object> registrar(@Valid @RequestBody Especialidad especialidad){
		
		Especialidad esp = new Especialidad();
		esp = service.registrar(especialidad);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(esp.getIdEspecialidad()).toUri();
		return ResponseEntity.created(location).build();
		
	}
	
	@PutMapping
	public ResponseEntity<Object> modificar(@Valid @RequestBody Especialidad especialidad){
		
		Especialidad esp = new Especialidad();
		esp = service.modificar(especialidad);
		return new ResponseEntity<Object>(HttpStatus.OK);
		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id){
		
		Especialidad esp = new Especialidad();
		esp = service.listarId(id);
		if(esp == null) {
			throw new ModeloNotFoundException("Id no encontrado: " + id);
		}else {
			service.eliminar(id);
		}
		
		return new ResponseEntity<Object>(HttpStatus.OK);
		
	}
}
