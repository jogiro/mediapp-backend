package com.jogiro.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.jogiro.exception.ModeloNotFoundException;
import com.jogiro.model.Examen;
import com.jogiro.service.IExamenService;

@RestController
@RequestMapping("/examenes")
public class ExamenController {

	@Autowired
	private IExamenService service;
	
	@GetMapping
	public ResponseEntity<List<Examen>> listar(){
		
		List<Examen> examenes = new ArrayList<>();
		examenes = service.listar();
		return new ResponseEntity<List<Examen>>(examenes, HttpStatus.OK);
		
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Examen> listarId(@PathVariable("id") Integer id){
		
		Examen examen = new Examen();
		examen = service.listarId(id);
		if(examen == null) {
			throw new ModeloNotFoundException("Id no encontrado: " + id);
		}
		
		return new ResponseEntity<Examen>(examen, HttpStatus.OK);
		
	}
	
	@PostMapping
	public ResponseEntity<Object> registrar(@Valid @RequestBody Examen examen){
		
		Examen exa = new Examen();
		exa = service.registrar(examen);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(exa.getIdExamen()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<Object> actualizar(@Valid @RequestBody Examen examen){
		
		Examen exa = new Examen();
		exa = service.modificar(examen);
		return new ResponseEntity<Object>(HttpStatus.OK);
		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id){
		
		Examen exa = new Examen();
		exa = service.listarId(id);
		if(exa == null) {
			throw new ModeloNotFoundException("Id no encontrado: " + id);
		}else {
			service.eliminar(id);
		}
				
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
}
