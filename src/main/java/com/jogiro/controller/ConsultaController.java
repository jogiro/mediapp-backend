package com.jogiro.controller;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.jogiro.dto.ConsultaListaExamenDTO;
import com.jogiro.dto.ConsultaResumenDTO;
import com.jogiro.dto.FiltroConsultaDTO;
import com.jogiro.exception.ModeloNotFoundException;
import com.jogiro.model.Archivo;
import com.jogiro.model.Consulta;
import com.jogiro.service.IArchvoService;
import com.jogiro.service.IConsultaService;

@RestController
@RequestMapping("/consultas")
public class ConsultaController {

	@Autowired
	private IConsultaService service;
	
	@Autowired
	private IArchvoService serviceArchivo;

	@GetMapping
	public ResponseEntity<List<Consulta>> listar() {
		List<Consulta> consultas = new ArrayList<>();
		consultas = service.listar();
		return new ResponseEntity<List<Consulta>>(consultas, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Consulta> listarPorId(@PathVariable("id") Integer id) {
		Consulta consulta = new Consulta();
		consulta = service.listarId(id);

		if (consulta == null) {
			throw new ModeloNotFoundException("Id no encontrado: " + id);
		}

		return new ResponseEntity<Consulta>(consulta, HttpStatus.OK);

	}

	@PostMapping
	public ResponseEntity<Object> registrar(@Valid @RequestBody ConsultaListaExamenDTO consultaListaExamenDTO) {
		Consulta cons = new Consulta();
		cons = service.registrarTransaccional(consultaListaExamenDTO);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(cons.getIdConsulta()).toUri();

		return ResponseEntity.created(location).build();
	}

	@PutMapping
	public ResponseEntity<Object> modificar(@Valid @RequestBody Consulta consulta) {
		Consulta cons = new Consulta();
		cons = service.modificar(consulta);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id) {
		Consulta cons = new Consulta();
		cons = service.listarId(id);
		if (cons == null) {
			throw new ModeloNotFoundException("Id no encontrado: " + id);
		} else {
			service.eliminar(id);
		}

		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@PostMapping("/buscar")
	public ResponseEntity<List<Consulta>> buscar(@RequestBody FiltroConsultaDTO filtro) {

		List<Consulta> consultas = new ArrayList<>();

		if (filtro != null) {
			if (filtro.getFechaConsulta() != null) {
				consultas = service.buscarFecha(filtro);
			} else {
				consultas = service.buscar(filtro);
			}
		}

		return new ResponseEntity<List<Consulta>>(consultas, HttpStatus.OK);

	}

	@GetMapping(value = "/listarResumen")
	public ResponseEntity<List<ConsultaResumenDTO>> listarResumen(){
		List<ConsultaResumenDTO> consultasResumen = new ArrayList<>();
		consultasResumen = service.listarResumen();
		return new ResponseEntity<List<ConsultaResumenDTO>>(consultasResumen, HttpStatus.OK);
	}

	@GetMapping(value = "/generarReporte", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporte() {
		byte[] data = null;
		data = service.generarReporte();
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	
	@PostMapping(value = "/guardarArchivo", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
	public ResponseEntity<Integer> guardarArchivo(@RequestParam("file") MultipartFile file) throws IOException{
		int rpta = 0;
		Archivo ar = new Archivo();
		ar.setFilename(file.getName());
		ar.setValue(file.getBytes());
		rpta = serviceArchivo.guardar(ar);
		
		return new ResponseEntity<Integer>(rpta, HttpStatus.OK);
	}
	
	@GetMapping(value = "/leerArchivo/{idArchivo}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> leerArchivo(@PathVariable("idArchivo") Integer idArchivo){
		
		Archivo ar = new Archivo();
		ar = serviceArchivo.leerArchivo(idArchivo);
		if(idArchivo == null) {
			throw new ModeloNotFoundException("Id no encontrado: " + idArchivo);
		}
		
		byte[] arr = ar.getValue();
		
		return new ResponseEntity<byte[]>(arr, HttpStatus.OK);
	}

}
