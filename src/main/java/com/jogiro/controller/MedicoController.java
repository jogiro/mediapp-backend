package com.jogiro.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.jogiro.exception.ModeloNotFoundException;
import com.jogiro.model.Medico;
import com.jogiro.service.IMedicoService;

@RestController
@RequestMapping("/medicos")
public class MedicoController {

	@Autowired
	private IMedicoService service;

	
	@GetMapping
	public ResponseEntity<List<Medico>> listar() {

		List<Medico> medicos = new ArrayList<>();
		medicos = service.listar();
		return new ResponseEntity<List<Medico>>(medicos, HttpStatus.OK);

	}

	
	//@PreAuthorize("@restAuthService.hasAccess('listar')")
	@GetMapping("/{id}")
	public ResponseEntity<Medico> listarId(@PathVariable("id") Integer id) {

		Medico medico = new Medico();
		medico = service.listarId(id);
		if (medico == null) {
			throw new ModeloNotFoundException("Id no encontrado: " + id);
		}

		return new ResponseEntity<Medico>(medico, HttpStatus.OK);

	}

	@PostMapping
	public ResponseEntity<Object> registrar(@Valid @RequestBody Medico medico) {

		Medico med = new Medico();
		med = service.registrar(medico);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(med.getIdMedico())
				.toUri();
		return ResponseEntity.created(location).build();
	}

	@PutMapping
	public ResponseEntity<Object> actualizar(@Valid @RequestBody Medico medico) {

		Medico med = new Medico();
		med = service.modificar(medico);
		return new ResponseEntity<Object>(HttpStatus.OK);

	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id){
		
		Medico med = new Medico();
		med = service.listarId(id);
		if(med == null) {
			throw new ModeloNotFoundException("Id no encontrado: " + id);
		}else {
			service.eliminar(id);
		}
		
		return new ResponseEntity<Object>(HttpStatus.OK);
		
	}

}
